
(use-modules
 (guix build-system cargo)
 (guix download)
 (guix gexp)
 (guix git-download)
 ((guix licenses) #:prefix license:)
 (guix packages)
 (gnu packages crates-io)
 (srfi srfi-1)
 (ice-9 popen)
 (ice-9 rdelim))

(define-public rust-filedescriptor-0.8
  (package
    (name "rust-filedescriptor")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "filedescriptor" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vplyh0cw35kzq7smmp2ablq0zsknk5rkvvrywqsqfrchmjxk6bi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/wez/wezterm")
    (synopsis "More ergonomic wrappers around RawFd and RawHandle")
    (description "More ergonomic wrappers around RawFd and RawHandle")
    (license license:expat)))

(define-public rust-gag-1
  (package
    (name "rust-gag")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gag" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wjr02svx7jir7b7r69lpfh3assasmqsz4vivzzzpsb677hvw4x7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-filedescriptor" ,rust-filedescriptor-0.8)
         ("rust-tempfile" ,rust-tempfile-3))
        #:cargo-development-inputs
        (("rust-dirs" ,rust-dirs-3) ("rust-lazy-static" ,rust-lazy-static-1))))
    (home-page "https://github.com/Stebalien/gag-rs")
    (synopsis
      "Gag, redirect, or hold stdout/stderr output. Currently only *nix operating systems are supported.")
    (description
      "Gag, redirect, or hold stdout/stderr output.  Currently only *nix operating
systems are supported.")
    (license license:expat)))

(define-public rascal-boot:devel
  (package
   (name "rascal-boot")
   (version
    ;; Git commit pipe hack borrowed from freebayes' guix.scm build file
    (git-version "1.2.0" "HEAD" (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f 2" OPEN_READ))))
   (source
    (local-file (dirname (current-filename)) #:recursive? #t))
   (build-system cargo-build-system)
   (arguments
    `(#:cargo-inputs
      (("rust-itertools" ,rust-itertools-0.10))
      #:cargo-development-inputs
      (("rust-gag" ,rust-gag-1))
      #:cargo-test-flags
      ;; Rascal-Boot's integration test suite uses the `gag` library to redirect stdout to a
      ;; tmpfile, which requires the --nocapture flag.
      "-- --nocapture"))
   (home-page "https://codeberg.org/unmatched-paren/rascal-boot")
   (synopsis "A Pascal interpreter in Rust")
   (description "Rascal-Boot is a fork of Rascal (https://github.com/tylerlaberge/rascal), a basic Pascal interpreter written in Rust, that focuses on extending Rascal to the point where it can bootstrap the Free Pascal Compiler (@code{fpc}).")
   (license license:expat)))

rascal-boot:devel
