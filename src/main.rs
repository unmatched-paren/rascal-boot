use std::{env, fs::File, io::Read};

fn main() -> Result<(), String> {
    let filename = get_file_name()?;
    let source = read_source_file(filename.as_str())?;

    rascal_boot::interpret(source);

    Ok(())
}

fn get_file_name() -> Result<String, String> {
    let mut args = env::args();
    args.next();

    args.next().ok_or(String::from("[UI Error] Missing filename"))
}

fn read_source_file(filename: &str) -> Result<String, String> {
    let mut file = File::open(filename).or(Err("[UI Error] Couldn't open file"))?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .or(Err("[UI Error] Could not read file"))?;

    Ok(contents)
}
