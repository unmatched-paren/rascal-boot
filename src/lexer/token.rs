//! Tokens are single units of syntax, intended to be arranged in a list. Their
//! purpose is to strip the source string of unnecessary noise, like whitespace.

/// An enumeration that represents a single Pascal token.
#[derive(Debug, PartialEq)]
pub enum Token {
    Program,
    Procedure,
    Function,
    Var,
    Comma,
    Colon,
    Integer,
    RealType,
    StringType,
    BooleanType,
    Begin,
    End,
    Dot,
    Semi,
    Assign,
    Id(String),
    Plus,
    Minus,
    Multiply,
    IntegerDiv,
    FloatDiv,
    And,
    Or,
    Not,
    LessThan,
    GreaterThan,
    Equal,
    NotEqual,
    LessOrEqual,
    GreaterOrEqual,
    Int(i32),
    Real(f32),
    String(String),
    Boolean(bool),
    If,
    Then,
    Else,
    LeftParen,
    RightParen,
    Eof,
}

#[derive(Debug)]
pub struct TokenCache {
    tokens: Vec<Token>,
}

impl TokenCache {
    /// Create a token cache.
    pub fn new() -> TokenCache {
        return TokenCache { tokens: vec![] };
    }

    ///
    pub fn push(&mut self, token: Token) {
        self.tokens.push(token);
    }

    pub fn peek(&self) -> Option<&Token> {
        return self.tokens.get(0);
    }

    pub fn peek_ahead(&self, ahead: usize) -> Option<&Token> {
        return self.tokens.get(ahead);
    }
}

impl Iterator for TokenCache {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        return if self.tokens.is_empty() {
            None
        } else {
            Some(self.tokens.remove(0))
        };
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn next() {
        let mut token_cache = TokenCache::new();

        token_cache.push(Token::Begin);
        token_cache.push(Token::End);

        assert_eq!(token_cache.next(), Some(Token::Begin));
        assert_eq!(token_cache.next(), Some(Token::End));
        assert_eq!(token_cache.next(), None)
    }

    #[test]
    fn peek() {
        let mut token_cache = TokenCache::new();

        token_cache.push(Token::Begin);
        token_cache.push(Token::End);

        assert_eq!(token_cache.peek(), Some(&Token::Begin));
        assert_eq!(token_cache.peek(), Some(&Token::Begin));
    }

    #[test]
    fn peek_ahead() {
        let mut token_cache = TokenCache::new();

        token_cache.push(Token::Begin);
        token_cache.push(Token::End);
        token_cache.push(Token::Dot);

        assert_eq!(token_cache.peek_ahead(0), Some(&Token::Begin));
        assert_eq!(token_cache.peek_ahead(1), Some(&Token::End));
        assert_eq!(token_cache.peek_ahead(2), Some(&Token::Dot));
        assert_eq!(token_cache.peek_ahead(3), None);
        assert_eq!(token_cache.peek_ahead(0), Some(&Token::Begin));
    }
}
