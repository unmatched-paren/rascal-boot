//! The lexer translates source code into a series of tokens.

mod lexer;
mod source;
mod token;

pub use lexer::Lexer;
pub use source::Source;
pub use token::Token;
