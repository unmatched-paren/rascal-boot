use super::{
    source::Source,
    token::{Token, TokenCache},
};
use itertools::Itertools;
use std::{
    io::{self, Write},
    process,
};

/// A Rascal source code lexer.
pub struct Lexer<'a> {
    source: Source<'a>,
    token_cache: TokenCache,
}

impl<'a> Lexer<'a> {
    /// Creates a new lexer with a token cache from source code.
    pub fn new(source: Source) -> Lexer {
        let token_cache = TokenCache::new();
        let mut lexer = Lexer {
            source,
            token_cache,
        };
        lexer.init();

        return lexer;
    }

    /// View the first token in the lexer's token cache.
    pub fn peek(&self) -> Option<&Token> {
        return self.token_cache.peek();
    }

    /// View the `ahead`th token in the lexer's token cache.
    pub fn peek_ahead(&self, ahead: usize) -> Option<&Token> {
        return self.token_cache.peek_ahead(ahead);
    }

    fn init(&mut self) {
        loop {
            match self.lex() {
                Ok(Token::Eof) => {
                    self.token_cache.push(Token::Eof);
                    break;
                }
                Ok(token) => self.token_cache.push(token),
                Err(error) => {
                    writeln!(io::stderr(), "{}", error).unwrap();
                    process::exit(1);
                }
            }
        }
    }

    fn number(&mut self) -> Result<i32, String> {
        let start_int: String = match self.source.current_char() {
            Some(c) if c.is_digit(10) => Ok(c),
            _ => Err("[Lexer Error] Expected number"),
        }?
        .to_string();

        let final_int = self
            .source
            .by_ref()
            .peeking_take_while(|c: &char| c.is_digit(10))
            .fold(start_int, |mut acc: String, next_int: char| {
                acc.push(next_int);
                return acc;
            })
            .parse::<i32>()
            .or(Err("[Lexer Error] Failed to parse integer"))?;

        return Ok(final_int);
    }

    fn integer(&mut self) -> Result<Token, String> {
        let integer = self.number()?;

        if let Some(&'.') = self.source.peek() {
            self.source.next(); // Eat the period

            let decimal = match self.source.next() {
                Some(c) if c.is_digit(10) => self.number(),
                Some(c) => Err(String::from(format!(
                    "[Lexer Error] Expected floating point number at: {}.{}",
                    integer, c
                ))),
                None => Err(String::from(format!(
                    "[Lexer Error] Expected floating point number at: {}.",
                    integer
                ))),
            }?;

            let mut string_real: String = integer.to_string();
            string_real.push('.');
            string_real.push_str(decimal.to_string().as_str());

            let real = string_real
                .parse::<f32>()
                .or(Err("[Lexer Error] Failed to parse float"))?;

            return Ok(Token::Real(real));
        } else {
            return Ok(Token::Int(integer));
        }
    }

    fn id(&mut self) -> Result<Token, String> {
        let start_id: String = match self.source.current_char() {
            Some(c) if c.is_alphabetic() => Ok(c),
            _ => Err("[Lexer Error] Expected alphabetic character"),
        }?
        .to_string();
        let final_id: String = self
            .source
            .by_ref()
            .peeking_take_while(|c: &char| c.is_alphanumeric() || c == &'_')
            .fold(start_id, |mut acc: String, next_id: char| {
                acc.push(next_id);
                return acc;
            });

        return match final_id.to_lowercase().as_str() {
            "program" => Ok(Token::Program),
            "procedure" => Ok(Token::Procedure),
            "function" => Ok(Token::Function),
            "begin" => Ok(Token::Begin),
            "end" => Ok(Token::End),
            "var" => Ok(Token::Var),
            "integer" => Ok(Token::Integer),
            "real" => Ok(Token::RealType),
            "string" => Ok(Token::StringType),
            "boolean" => Ok(Token::BooleanType),
            "true" => Ok(Token::Boolean(true)),
            "false" => Ok(Token::Boolean(false)),
            "and" => Ok(Token::And),
            "or" => Ok(Token::Or),
            "not" => Ok(Token::Not),
            "div" => Ok(Token::IntegerDiv),
            "if" => Ok(Token::If),
            "then" => Ok(Token::Then),
            "else" => Ok(Token::Else),
            _ => Ok(Token::Id(final_id)),
        };
    }

    fn string(&mut self) -> Result<Token, String> {
        if let Some('\'') = self.source.current_char() {
            Ok(())
        } else {
            Err("[Lexer Error] Expected '\'' character")
        }?;

        let final_string: String = self
            .source
            .by_ref()
            .peeking_take_while(|c: &char| c != &'\'')
            .fold(String::from(""), |mut acc: String, next_char: char| {
                acc.push(next_char);
                return acc;
            });

        if let Some('\'') = self.source.next() {
            Ok(())
        } else {
            Err("[Lexer Error] Expected '\'' character")
        }?;
        return Ok(Token::String(final_string));
    }

    fn assign(&mut self) -> Result<Token, String> {
        if let (Some(':'), Some('=')) = (self.source.current_char(), self.source.next()) {
            Ok(Token::Assign)
        } else {
            Err(String::from("[Lexer Error] Expected ':=' characters"))
        }
    }

    fn comparison(&mut self) -> Result<Token, String> {
        return match self.source.current_char() {
            Some('<') => match self.source.peek() {
                Some('=') => {
                    self.source.next();
                    Ok(Token::LessOrEqual)
                }
                Some('>') => {
                    self.source.next();
                    Ok(Token::NotEqual)
                }
                _ => Ok(Token::LessThan),
            },
            Some('>') => {
                if let Some('=') = self.source.peek() {
                    self.source.next();
                    Ok(Token::GreaterOrEqual)
                } else {
                    Ok(Token::GreaterThan)
                }
            }
            Some('=') => Ok(Token::Equal),
            _ => Err(String::from("[Lexer Error] Expected comparison character")),
        };
    }

    fn lex(&mut self) -> Result<Token, String> {
        return match self.source.next() {
            Some(character) if character.is_whitespace() => self.lex(),
            Some(character) if character.is_digit(10) => self.integer(),
            Some(character) if character.is_alphabetic() => self.id(),
            Some(':') => {
                if let Some('=') = self.source.peek() {
                    self.assign()
                } else {
                    Ok(Token::Colon)
                }
            }
            Some('\'') => self.string(),
            Some('<') => self.comparison(),
            Some('>') => self.comparison(),
            Some('=') => self.comparison(),
            Some(',') => Ok(Token::Comma),
            Some('.') => Ok(Token::Dot),
            Some(';') => Ok(Token::Semi),
            Some('+') => Ok(Token::Plus),
            Some('-') => Ok(Token::Minus),
            Some('*') => Ok(Token::Multiply),
            Some('/') => Ok(Token::FloatDiv),
            Some('(') => Ok(Token::LeftParen),
            Some(')') => Ok(Token::RightParen),
            None => Ok(Token::Eof),
            Some(character) => Err(format!("[Lexer Error] Unknown Token '{}'", character)),
        };
    }
}

impl<'a> Iterator for Lexer<'a> {
    type Item = Token;

    fn next(&mut self) -> Option<Self::Item> {
        return self.token_cache.next();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn program_token() {
        let source = Source::new("program");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Program));
    }

    #[test]
    fn procedure_token() {
        let source = Source::new("procedure");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Procedure));
    }

    #[test]
    fn function_token() {
        let source = Source::new("function");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Function));
    }

    #[test]
    fn var_token() {
        let source = Source::new("var");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Var));
    }

    #[test]
    fn comma_token() {
        let source = Source::new(",");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Comma));
    }

    #[test]
    fn colon_token() {
        let source = Source::new(":");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Colon));
    }

    #[test]
    fn integer_token() {
        let source = Source::new("integer");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Integer));
    }

    #[test]
    fn real_token() {
        let source = Source::new("real");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::RealType));
    }

    #[test]
    fn string_token() {
        let source = Source::new("string");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::StringType));
    }

    #[test]
    fn boolean_token() {
        let source = Source::new("boolean");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::BooleanType));
    }

    #[test]
    fn begin_token() {
        let source = Source::new("begin");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Begin));
    }

    #[test]
    fn end_token() {
        let source = Source::new("end");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::End));
    }

    #[test]
    fn dot_token() {
        let source = Source::new(".");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Dot));
    }

    #[test]
    fn semi_token() {
        let source = Source::new(";");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Semi));
    }

    #[test]
    fn assign_token() {
        let source = Source::new(":=");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Assign));
    }

    #[test]
    fn id_token() {
        let source = Source::new("foobar");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Id(String::from("foobar"))));
    }

    #[test]
    fn plus_token() {
        let source = Source::new("+");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Plus));
    }

    #[test]
    fn minus_token() {
        let source = Source::new("-");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Minus));
    }

    #[test]
    fn multiply_token() {
        let source = Source::new("*");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Multiply));
    }

    #[test]
    fn integer_div_token() {
        let source = Source::new("div");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::IntegerDiv));
    }

    #[test]
    fn float_div_token() {
        let source = Source::new("/");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::FloatDiv));
    }

    #[test]
    fn and_token() {
        let source = Source::new("and");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::And));
    }

    #[test]
    fn or_token() {
        let source = Source::new("or");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Or));
    }

    #[test]
    fn not_token() {
        let source = Source::new("not");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Not));
    }

    #[test]
    fn less_than_token() {
        let source = Source::new("<");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::LessThan));
    }

    #[test]
    fn greater_than_token() {
        let source = Source::new(">");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::GreaterThan));
    }

    #[test]
    fn equal_token() {
        let source = Source::new("=");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Equal));
    }

    #[test]
    fn not_equal_token() {
        let source = Source::new("<>");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::NotEqual));
    }

    #[test]
    fn less_than_or_equal_token() {
        let source = Source::new("<=");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::LessOrEqual));
    }

    #[test]
    fn greater_than_or_equal_token() {
        let source = Source::new(">=");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::GreaterOrEqual));
    }

    #[test]
    fn integer_const_token() {
        let source = Source::new("5");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Int(5)));
    }

    #[test]
    fn real_const_token() {
        let source = Source::new("5.5");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Real(5.5)));
    }

    #[test]
    fn string_literal_token() {
        let source = Source::new("'hello world'");
        let mut lexer = Lexer::new(source);

        assert_eq!(
            lexer.next(),
            Some(Token::String(String::from("hello world")))
        );
    }

    #[test]
    fn boolean_const_token() {
        let source = Source::new("true");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Boolean(true)));
    }

    #[test]
    fn if_token() {
        let source = Source::new("if");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::If));
    }

    #[test]
    fn then_token() {
        let source = Source::new("then");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Then));
    }

    #[test]
    fn else_token() {
        let source = Source::new("else");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Else));
    }

    #[test]
    fn left_paren_token() {
        let source = Source::new("(");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::LeftParen));
    }

    #[test]
    fn right_paren_token() {
        let source = Source::new(")");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::RightParen));
    }

    #[test]
    fn eof_token() {
        let source = Source::new("");
        let mut lexer = Lexer::new(source);

        assert_eq!(lexer.next(), Some(Token::Eof));
    }
}
