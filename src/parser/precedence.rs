pub enum Precedence {
    Call = 7,
    UnaryNum = 6,
    Product = 5,
    Sum = 4,
    Comparison = 3,
    UnaryBool = 2,
    BinaryBool = 1,
}
