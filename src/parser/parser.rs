use super::{
    ast::{
        Assignment, Block, CallParameters, Compound, Declarations, Expr, FormalParameterList,
        FormalParameters, FunctionCall, FunctionDeclaration, IfStatement, ProcedureDeclaration,
        Program, Statement, TypeSpec, Variable, VariableDeclaration,
    },
    parselet::{InfixParselet, PrefixParselet},
    precedence::Precedence,
};
use crate::lexer::{Lexer, Token};

///
/// <pre>
///     program               :: Program variable Semi block Dot
///     block                 :: declarations compound_statement
///     declarations          :: Var (variable_declaration)+ (procedure_declaration | function_declaration)* | (procedure_declaration | function_declaration)* | empty
///     procedure_declaration :: Procedure Id (LeftParen formal_parameter_list RightParen)? Semi block
///     function_declaration  :: Function Id LeftParen formal_parameter_list RightParen Colon type_spec Semi block
///     formal_parameter_list :: formal_parameters | formal_parameters Semi formal_parameter_list | empty
///     formal_parameters     :: Id (Comma Id)* Colon type_spec
///     variable_declaration  :: ID (COMMA ID)* Colon type_spec Semi
///     type_spec             :: IntegerType | RealType | BooleanType
///     compound_statement    :: Begin (statement)* End
///     statement             :: compound_statement | if_statement | assignment_statement | function_call
///     if_statement          :: If expr Then compound_statement (Else (if_statement | compound_statement))?
///     assignment_statement  :: variable Assign expr Semi
///     variable              :: Id
///     function_call         :: variable LeftParen (call_parameters)? RightParen Semi
///     call_parameters       :: expr (Comma expr)* | empty
///     expr                  :: unaryop_expr | binop_expr | grouped_expr | function_call | literal | variable
///     unaryop_expr          :: unaryop expr
///     unaryop               :: Plus | Minus | Not
///     binop_expr            :: binop expr
///     binop                 :: Plus | Minus | Mul | IntegerDiv | FloatDiv | And | Or |
///                              LessThan | LessOrEqual | GreaterThan |
///                              GreaterOrEqual | Equal | NotEqual
///     grouped_expr          :: LeftParen expr RightParen
///     literal               :: Integer | Real | Boolean | String
///     empty                 ::
/// </pre>
pub struct Parser<'a> {
    pub lexer: Lexer<'a>,
}

impl<'a> Parser<'a> {
    pub fn new(lexer: Lexer<'a>) -> Parser<'a> {
        Parser { lexer }
    }

    fn get_prefix_parselet(token: &Token) -> Result<PrefixParselet, String> {
        match token {
            Token::Int(_) | Token::Real(_) | Token::String(_) | Token::Boolean(_) => {
                Ok(PrefixParselet::Literal)
            }
            Token::Plus | Token::Minus => {
                Ok(PrefixParselet::UnaryOperator(Precedence::UnaryNum as u32))
            }
            Token::Not => Ok(PrefixParselet::UnaryOperator(Precedence::UnaryBool as u32)),
            Token::LeftParen => Ok(PrefixParselet::Grouping),
            Token::Id(_) => Ok(PrefixParselet::Variable),
            _ => Err(String::from(format!(
                "[Parse Error (Expression)] at token {:?}",
                token
            ))),
        }
    }

    fn get_infix_parselet(token: &Token) -> Result<InfixParselet, String> {
        match token {
            Token::Plus | Token::Minus => Ok(InfixParselet::BinaryOperator(Precedence::Sum as u32)),
            Token::Multiply | Token::IntegerDiv | Token::FloatDiv => {
                Ok(InfixParselet::BinaryOperator(Precedence::Product as u32))
            }
            Token::And | Token::Or => {
                Ok(InfixParselet::BinaryOperator(Precedence::BinaryBool as u32))
            }
            Token::LessThan
            | Token::LessOrEqual
            | Token::GreaterThan
            | Token::GreaterOrEqual
            | Token::Equal
            | Token::NotEqual => Ok(InfixParselet::BinaryOperator(Precedence::Comparison as u32)),
            Token::LeftParen => Ok(InfixParselet::FunctionCall(Precedence::Call as u32)),
            _ => Err(String::from(format!(
                "[Parse Error (Expression)] at token {:?}",
                token
            ))),
        }
    }

    fn get_next_precedence(&mut self) -> u32 {
        if let Some(token) = self.lexer.peek() {
            if let Ok(parselet) = Parser::get_infix_parselet(token) {
                parselet.get_precedence()
            } else {
                0
            }
        } else {
            0
        }
    }

    /// <pre>
    ///     program :: Program variable Semi block Dot
    /// </pre>
    pub fn program(&mut self) -> Result<Program, String> {
        if let Some(Token::Program) = self.lexer.next() {
            Ok(())
        } else {
            Err(String::from("[Parse Error]: Expected token Program"))
        }?;
        let variable = self.variable()?;
        if let Some(Token::Semi) = self.lexer.next() {
            Ok(())
        } else {
            Err(String::from(format!(
                "[Parse Error @ {:?}] Expected token {:?}",
                variable,
                Token::Semi
            )))
        }?;
        let block = self.block()?;
        if let Some(Token::Dot) = self.lexer.next() {
            Ok(())
        } else {
            Err(String::from(format!(
                "[Parse Error] Expected token {:?}",
                Token::Dot
            )))
        }?;

        Ok(Program(variable, block))
    }

    /// <pre>
    ///     block :: declarations compound_statement
    /// </pre>
    pub fn block(&mut self) -> Result<Block, String> {
        Ok(Block(self.declarations()?, self.compound_statement()?))
    }

    /// <pre>
    ///     declarations :: Var (variable_declaration)+ (procedure_declaration | function_declaration)* | (procedure_declaration | function_declaration)* | empty
    /// </pre>
    pub fn declarations(&mut self) -> Result<Vec<Declarations>, String> {
        let mut declarations: Vec<Declarations> = vec![];

        if let Some(&Token::Var) = self.lexer.peek() {
            self.lexer.next(); // eat the var

            let mut variable_declarations: Vec<VariableDeclaration> = vec![];
            while let Some(&Token::Id(_)) = self.lexer.peek() {
                variable_declarations.push(self.variable_declaration()?);
            }

            if variable_declarations.len() > 0 {
                declarations.push(Declarations::VariableDeclarations(variable_declarations));
            } else {
                return Err(String::from(format!("[Parse Error (Declarations)] Expected at least one variable declaration after token {:?}", Token::Var)));
            }
        }

        let mut procedure_declarations: Vec<ProcedureDeclaration> = vec![];
        let mut function_declarations: Vec<FunctionDeclaration> = vec![];

        loop {
            match self.lexer.peek() {
                Some(Token::Procedure) => {
                    procedure_declarations.push(self.procedure_declaration()?)
                }
                Some(Token::Function) => function_declarations.push(self.function_declaration()?),
                _ => break,
            }
        }

        if procedure_declarations.len() > 0 {
            declarations.push(Declarations::ProcedureDeclarations(procedure_declarations));
        }

        if function_declarations.len() > 0 {
            declarations.push(Declarations::FunctionDeclarations(function_declarations));
        }

        if declarations.is_empty() {
            declarations.push(Declarations::Empty);
        }

        Ok(declarations)
    }

    /// <pre>
    ///     variable_declaration :: Id (Comma Id)* Colon type_spec Semi
    /// </pre>
    fn variable_declaration(&mut self) -> Result<VariableDeclaration, String> {
        let mut ids: Vec<String> = vec![];

        if let Some(Token::Id(name)) = self.lexer.next() {
            ids.push(name);
            Ok(())
        } else {
            Err(String::from(format!(
                "[Parse Error (Variable Declaration)] Expected id token"
            )))
        }?;

        while let Some(Token::Comma) = self.lexer.peek() {
            self.lexer.next(); // eat the comma

            if let Some(Token::Id(name)) = self.lexer.next() {
                ids.push(name);
                Ok(())
            } else {
                Err(String::from(format!(
                    "[Parse Error (Variable Declaration)] Expected id token after token {:?}",
                    Token::Comma
                )))
            }?;
        }

        if let Some(Token::Colon) = self.lexer.next() {
            match (self.type_spec()?, self.lexer.next()) {
                (type_spec, Some(Token::Semi)) => Ok(VariableDeclaration(ids, type_spec)),
                (type_spec, _) => Err(String::from(format!(
                    "[Parse Error (Variable Declaration)] Expected {:?} token after {:?}",
                    Token::Semi,
                    type_spec
                ))),
            }
        } else {
            Err(String::from(format!(
                "[Parse Error (Variable Declaration)]: Expected {:?} token after declared variables",
                Token::Colon
            )))
        }
    }

    /// <pre>
    ///     type_spec:: Integer | Real | String | BooleanType
    /// </pre>
    fn type_spec(&mut self) -> Result<TypeSpec, String> {
        match self.lexer.next() {
            Some(Token::Integer) => Ok(TypeSpec::Integer),
            Some(Token::RealType) => Ok(TypeSpec::Real),
            Some(Token::StringType) => Ok(TypeSpec::String),
            Some(Token::BooleanType) => Ok(TypeSpec::Boolean),
            _ => Err(String::from(format!(
                "[Parse Error (Type Specification)] Expected one of {:?}",
                vec![
                    TypeSpec::Integer,
                    TypeSpec::Real,
                    TypeSpec::String,
                    TypeSpec::Boolean
                ]
            ))),
        }
    }

    /// <pre>
    ///     procedure_declaration :: Procedure Id (LeftParen formal_parameter_list RightParen)? Semi block
    /// </pre>
    fn procedure_declaration(&mut self) -> Result<ProcedureDeclaration, String> {
        let name = if let (Some(Token::Procedure), Some(Token::Id(name))) =
            (self.lexer.next(), self.lexer.next())
        {
            Ok(name)
        } else {
            Err(String::from(format!(
                "[Parse Error (Procedure Declaration)] Expected {:?} <id>",
                Token::Procedure
            )))
        }?;

        let parameters = if let Some(Token::LeftParen) = self.lexer.peek() {
            self.lexer.next(); // eat the LeftParen
            match (self.formal_parameter_list()?, self.lexer.next()) {
                (formal_parameter_list, Some(Token::RightParen)) => Ok(formal_parameter_list),
                (formal_parameter_list, _) => Err(format!(
                    "[Parse Error (Procedure Declaration)] Expected {:?} after {:?}",
                    Token::RightParen,
                    formal_parameter_list
                )),
            }
        } else {
            Ok(FormalParameterList(vec![]))
        }?;

        let block = if let Some(Token::Semi) = self.lexer.next() {
            self.block()
        } else {
            Err(String::from(format!(
                "[Parse Error (Procedure Declaration)] Expected token {:?} after {:?}",
                Token::Semi,
                parameters
            )))
        }?;

        Ok(ProcedureDeclaration(name, parameters, block))
    }

    /// <pre>
    ///     function_declaration :: Function Id LeftParen formal_parameter_list RightParen Colon type_spec Semi block
    /// </pre>
    fn function_declaration(&mut self) -> Result<FunctionDeclaration, String> {
        let name = if let (Some(Token::Function), Some(Token::Id(name))) =
            (self.lexer.next(), self.lexer.next())
        {
            Ok(name)
        } else {
            Err(String::from(format!(
                "[Parse Error (Function Declaration)] Expected {:?} <id>",
                Token::Function
            )))
        }?;
        let parameters = if let Some(Token::LeftParen) = self.lexer.next() {
            match (self.formal_parameter_list()?, self.lexer.next()) {
                (formal_parameter_list, Some(Token::RightParen)) => Ok(formal_parameter_list),
                (formal_parameter_list, _) => Err(format!(
                    "[Parse Error (Function Declaration)] Expected {:?} after {:?}",
                    Token::RightParen,
                    formal_parameter_list
                )),
            }
        } else {
            Err(format!(
                "[Parse Error (Function Declaration)] Expected {:?} after '{:?} {:?}'",
                Token::LeftParen,
                Token::Function,
                name
            ))
        }?;
        let return_type = if let Some(Token::Colon) = self.lexer.next() {
            self.type_spec()
        } else {
            Err(String::from(format!(
                "[Parse Error (Function Declaration)] Expected {:?} after {:?}",
                Token::Colon,
                parameters
            )))
        }?;
        let block = if let Some(Token::Semi) = self.lexer.next() {
            self.block()
        } else {
            Err(String::from(format!(
                "[Parse Error (Function Declaration)] Expected {:?} after {:?}",
                Token::Semi,
                return_type
            )))
        }?;

        Ok(FunctionDeclaration(name, parameters, block, return_type))
    }

    /// <pre>
    ///     formal_parameter_list :: formal_parameters | formal_parameters Semi formal_parameter_list | empty
    /// </pre>
    fn formal_parameter_list(&mut self) -> Result<FormalParameterList, String> {
        let mut parameters: Vec<FormalParameters> = vec![];

        if let Some(Token::Id(_)) = self.lexer.peek() {
            loop {
                parameters.push(self.formal_parameters()?);

                if let Some(Token::Semi) = self.lexer.peek() {
                    self.lexer.next()
                } else {
                    break;
                };
            }
        }

        Ok(FormalParameterList(parameters))
    }

    /// <pre>
    ///     formal_parameters :: Id (Comma Id)* Colon type_spec
    /// </pre>
    fn formal_parameters(&mut self) -> Result<FormalParameters, String> {
        let mut ids: Vec<String> = vec![];

        match self.lexer.next() {
            Some(Token::Id(name)) => {
                ids.push(name);
                Ok(())
            }
            token => Err(String::from(format!(
                "[Parser Error (Formal Parameters)] Expected id token, got token {:?}",
                token
            ))),
        }?;

        while let Some(Token::Comma) = self.lexer.peek() {
            self.lexer.next(); // eat the comma

            if let Some(Token::Id(name)) = self.lexer.next() {
                ids.push(name);
                Ok(())
            } else {
                Err(String::from(format!(
                    "[Parser Error (Formal Parameters)] Expected id token after {:?}",
                    Token::Comma
                )))
            }?;
        }

        let type_spec = if let Some(Token::Colon) = self.lexer.next() {
            self.type_spec()
        } else {
            Err(String::from(format!(
                "[Parser Error (Formal Parameters)] Expected token {:?} after parameter identifiers",
                Token::Colon
            )))
        }?;

        Ok(FormalParameters(ids, type_spec))
    }

    /// <pre>
    ///     compound_statement :: Begin (statement)* End
    /// </pre>
    fn compound_statement(&mut self) -> Result<Compound, String> {
        if let Some(Token::Begin) = self.lexer.next() {
            Ok(())
        } else {
            Err(String::from(format!(
                "[Parse Error (Compound Statement)] Expected token {:?}",
                Token::Begin
            )))
        }?;

        let mut statements: Vec<Statement> = vec![];
        while !std::matches!(self.lexer.peek(), Some(Token::End)) {
            statements.push(self.statement()?);
        }
        self.lexer.next(); // eat the 'END' token

        Ok(Compound(statements))
    }

    /// <pre>
    ///     statement :: compound_statement | if_statement | assignment_statement | function_call
    /// </pre>
    fn statement(&mut self) -> Result<Statement, String> {
        match self.lexer.peek() {
            Some(Token::Begin) => Ok(Statement::Compound(self.compound_statement()?)),
            Some(Token::Id(_)) => match self.lexer.peek_ahead(1) {
                Some(Token::LeftParen) => Ok(Statement::FunctionCall(self.function_call()?)),
                Some(Token::Assign) => Ok(Statement::Assignment(self.assignment_statement()?)),
                _                   => Err(String::from("[Parse Error (Statement)] Expected assignment statement or function call after id token"))
            },
            Some(Token::If)    => Ok(Statement::IfStatement(self.if_statement()?)),
            _                  => Err(String::from("[Parse Error (Statement)] Expected compound statement, function call, assignment statement, or if statement"))
        }
    }

    /// <pre>
    ///     if_statement :: If expr Then compound_statement (Else (if_statement | compound_statement))?
    /// </pre>
    fn if_statement(&mut self) -> Result<IfStatement, String> {
        let (if_expr, if_compound) = if let Some(Token::If) = self.lexer.next() {
            match (self.expr(None)?, self.lexer.next()) {
                (expr, Some(Token::Then)) => Ok((expr, self.compound_statement()?)),
                (expr, _) => Err(String::from(format!(
                    "[Parse Error (If Statement)] Expected token {:?} after {:?}",
                    Token::Then,
                    expr
                ))),
            }
        } else {
            Err(String::from(format!(
                "[Parse Error (If Statement)] Expected token {:?}",
                Token::If
            )))
        }?;

        if let Some(Token::Else) = self.lexer.peek() {
            self.lexer.next(); // eat the 'else' token
            if let Some(Token::If) = self.lexer.peek() {
                Ok(IfStatement::IfElseIf(
                    if_expr,
                    if_compound,
                    Box::new(self.if_statement()?),
                ))
            } else {
                Ok(IfStatement::IfElse(
                    if_expr,
                    if_compound,
                    self.compound_statement()?,
                ))
            }
        } else {
            Ok(IfStatement::If(if_expr, if_compound))
        }
    }

    /// <pre>
    ///     assignment_statement  :: variable Assign expr Semi
    /// </pre>
    fn assignment_statement(&mut self) -> Result<Assignment, String> {
        match (self.variable()?, self.lexer.next()) {
            (var, Some(Token::Assign)) => match (self.expr(None)?, self.lexer.next()) {
                (expr, Some(Token::Semi)) => Ok(Assignment(var, expr)),
                (expr, _) => Err(String::from(format!(
                    "[Parse Error (Assignment Statement)] Expected {:?} after {:?}",
                    Token::Semi,
                    expr
                ))),
            },
            (var, _) => Err(String::from(format!(
                "[Parse Error (Assignment Statement)] Expected {:?} after {:?}",
                Token::Assign,
                var
            ))),
        }
    }

    /// <pre>
    ///     variable :: Id
    /// </pre>
    fn variable(&mut self) -> Result<Variable, String> {
        if let Some(Token::Id(id)) = self.lexer.next() {
            Ok(Variable(id))
        } else {
            Err(String::from("[Parse Error (Variable)]: Expected id token"))
        }
    }

    /// <pre>
    ///     function_call :: variable LeftParen (call_parameters)? RightParen Semi
    /// </pre>
    pub fn function_call(&mut self) -> Result<FunctionCall, String> {
        let function_id = match (self.variable()?, self.lexer.next()) {
            (variable, Some(Token::LeftParen)) => Ok(variable),
            (variable, _) => Err(String::from(format!(
                "[Parse Error (Function Call)]: Expected token {:?} after {:?}",
                Token::LeftParen,
                variable
            ))),
        }?;

        match (
            self.call_parameters()?,
            self.lexer.next(),
            self.lexer.next(),
        ) {
            (call_parameters, Some(Token::RightParen), Some(Token::Semi)) => {
                Ok(FunctionCall(function_id, call_parameters))
            }
            (call_parameters, _, _) => Err(String::from(format!(
                "[Parse Error (Function Call)]: Expected tokens {:?} {:?} after {:?}",
                Token::RightParen,
                Token::Semi,
                call_parameters
            ))),
        }
    }

    /// <pre>
    ///     call_parameters :: expr (Comma expr)* | empty
    /// </pre>
    pub fn call_parameters(&mut self) -> Result<CallParameters, String> {
        if let Some(&Token::RightParen) = self.lexer.peek() {
            Ok(CallParameters(vec![]))
        } else {
            let mut parameters = vec![self.expr(None)?];

            while let Some(&Token::Comma) = self.lexer.peek() {
                self.lexer.next(); // eat the comma
                parameters.push(self.expr(None)?);
            }

            Ok(CallParameters(parameters))
        }
    }

    pub fn expr(&mut self, precedence: Option<u32>) -> Result<Expr, String> {
        let precedence = precedence.unwrap_or(0);
        let token = self.lexer.next().ok_or(String::from(
            "[Parse Error (Expression)] Expected a token, found none",
        ))?;
        let parselet = Parser::get_prefix_parselet(&token)?;

        let mut left = parselet.parse(self, &token)?;

        while precedence < self.get_next_precedence() {
            let token = self.lexer.next().ok_or(String::from(
                "[Parse Error(Expression)] Expected a token, found none",
            ))?;
            let parselet = Parser::get_infix_parselet(&token)?;

            left = parselet.parse(self, &left, &token)?;
        }

        Ok(left)
    }

    pub fn parse(&mut self) -> Result<Program, String> {
        let program = self.program()?;

        match self.lexer.next() {
            Some(Token::Eof) => Ok(()),
            Some(token) => Err(String::from(format!(
                "[Parse Error] Expected {:?} token, found token {:?}",
                Token::Eof,
                token
            ))),
            None => Err(String::from(format!(
                "[Parse Error] Expected {:?} token, found None",
                Token::Eof
            ))),
        }?;

        Ok(program)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        lexer::Source,
        parser::ast::{BinaryOpExpr, BinaryOperator, GroupedExpr, UnaryOpExpr, UnaryOperator},
    };

    fn get_parser(text: &str) -> Parser {
        Parser::new(Lexer::new(Source::new(text)))
    }

    #[test]
    fn program() {
        let mut parser = get_parser("program test; begin end.");

        assert_matches!(parser.program(), Ok(Program(_, _)));
    }

    #[test]
    fn block() {
        let mut parser = get_parser("begin end");

        assert_matches!(parser.block(), Ok(Block(_, _)));
    }

    #[test]
    fn declarations() {
        let mut parser = get_parser("var test_var: integer; function test_func(): string; begin end procedure test_proc; begin end");
        match parser.declarations() {
            Ok(declarations) => {
                assert_eq!(declarations.len(), 3);
                assert_matches!(declarations[0], Declarations::VariableDeclarations(_));
                assert_matches!(declarations[1], Declarations::ProcedureDeclarations(_));
                assert_matches!(declarations[2], Declarations::FunctionDeclarations(_));
            }
            Err(e) => panic!("{}", e),
        };
    }

    #[test]
    fn variable_declaration() {
        let mut parser = get_parser("foo, bar, baz : integer;");
        match parser.variable_declaration() {
            Ok(VariableDeclaration(names, _)) => assert_eq!(
                names,
                vec![
                    String::from("foo"),
                    String::from("bar"),
                    String::from("baz")
                ]
            ),
            Err(e) => panic!("{}", e),
        }
    }

    #[test]
    fn type_spec() {
        let mut parser = get_parser("integer real string boolean");

        assert_matches!(parser.type_spec(), Ok(TypeSpec::Integer));
        assert_matches!(parser.type_spec(), Ok(TypeSpec::Real));
        assert_matches!(parser.type_spec(), Ok(TypeSpec::String));
        assert_matches!(parser.type_spec(), Ok(TypeSpec::Boolean));
    }

    #[test]
    fn procedure_declaration() {
        let mut parser = get_parser("procedure test(); begin end");
        match parser.procedure_declaration() {
            Ok(ProcedureDeclaration(name, _, _)) => assert_eq!(name, String::from("test")),
            Err(e) => panic!("{}", e),
        }
    }

    #[test]
    fn function_declaration() {
        let mut parser = get_parser("function test(): integer; begin end");
        match parser.function_declaration() {
            Ok(FunctionDeclaration(name, _, _, _)) => assert_eq!(name, String::from("test")),
            Err(e) => panic!("{}", e),
        }
    }

    #[test]
    fn formal_parameters_list() {
        let mut parser = get_parser("a: integer; b: real; c, d: string");

        assert_matches!(parser.formal_parameter_list(), Ok(FormalParameterList(_)));
    }

    #[test]
    fn formal_parameters() {
        let mut parser = get_parser("a, b, c: integer");
        match parser.formal_parameters() {
            Ok(FormalParameters(names, _)) => assert_eq!(
                names,
                vec![String::from("a"), String::from("b"), String::from("c")]
            ),
            Err(e) => panic!("{}", e),
        };
    }

    #[test]
    fn compound_statement() {
        let mut parser = get_parser("begin end");

        assert_matches!(parser.compound_statement(), Ok(Compound(_)));
    }

    #[test]
    fn if_statement() {
        let mut parser = get_parser("if true then begin end");

        assert_matches!(parser.if_statement(), Ok(IfStatement::If(_, _)));
    }

    #[test]
    fn if_else_statement() {
        let mut parser = get_parser("if false then begin end else begin end");

        assert_matches!(parser.if_statement(), Ok(IfStatement::IfElse(_, _, _)));
    }

    #[test]
    fn if_else_if_statement() {
        let mut parser = get_parser(
            "if false then begin end else if true then begin end else if true then begin end",
        );
        if let Ok(IfStatement::IfElseIf(_, _, else_if_one)) = parser.if_statement() {
            if let IfStatement::IfElseIf(_, _, else_if_two) = *else_if_one {
                match *else_if_two {
                    IfStatement::If(_, _) => (),
                    _ => panic!("Failure: Expected IfStatement::If AST node"),
                }
            } else {
                panic!("Failure: Expected IfStatement::IfElseIf AST node")
            }
        } else {
            panic!("Failure: Expected IfStatement::IfElseIf AST node")
        };
    }

    #[test]
    fn if_else_if_else_statement() {
        let mut parser =
            get_parser("if false then begin end else if false then begin end else begin end");
        if let Ok(IfStatement::IfElseIf(_, _, else_if)) = parser.if_statement() {
            match *else_if {
                IfStatement::IfElse(_, _, _) => (),
                _ => panic!("Failure: Expected IfStatement::IfElse AST node"),
            }
        } else {
            panic!("Failure: Expected IfStatement::IfElseIf AST node")
        };
    }

    #[test]
    fn assignment_statement() {
        let mut parser = get_parser("foo := 5;");

        assert_matches!(parser.assignment_statement(), Ok(Assignment(_, _)));
    }

    #[test]
    fn variable() {
        let mut parser = get_parser("foo");
        match parser.variable() {
            Ok(Variable(name)) => assert_eq!(name, String::from("foo")),
            Err(e) => panic!("{}", e),
        };
    }

    #[test]
    fn function_call() {
        let mut parser = get_parser("test(foo, bar, 5 + 5);");

        assert_matches!(parser.function_call(), Ok(FunctionCall(_, _)));
    }

    #[test]
    fn call_parameters() {
        let mut parser = get_parser("foo, bar, 5 + 5");

        assert_matches!(parser.call_parameters(), Ok(CallParameters(_)));
    }

    #[test]
    fn expr_unary_plus() {
        let mut parser = get_parser("+5");
        match parser.expr(Some(0)) {
            Ok(Expr::UnaryOp(unary_expr)) => match *unary_expr {
                UnaryOpExpr(UnaryOperator::Plus, _) => (),
                _ => panic!("Failure: Expected unary plus AST node"),
            },
            _ => panic!("Failure: Expected unary expression AST node"),
        };
    }

    #[test]
    fn expr_unary_minus() {
        let mut parser = get_parser("-5");
        match parser.expr(Some(0)) {
            Ok(Expr::UnaryOp(unary_expr)) => match *unary_expr {
                UnaryOpExpr(UnaryOperator::Minus, _) => (),
                _ => panic!("Failure: Expected unary minus AST node"),
            },
            _ => panic!("Failure: Expected unary expression AST node"),
        };
    }

    #[test]
    fn expr_binary_add() {
        let mut parser = get_parser("5 + 5");
        match parser.expr(Some(0)) {
            Ok(Expr::BinOp(binop_expr)) => match *binop_expr {
                BinaryOpExpr(_, BinaryOperator::Plus, _) => (),
                _ => panic!("Failure: Expected binary addition AST node"),
            },
            _ => panic!("Failure: Expected binary expression AST node"),
        };
    }

    #[test]
    fn expr_binary_subtract() {
        let mut parser = get_parser("5 - 5");
        match parser.expr(Some(0)) {
            Ok(Expr::BinOp(binop_expr)) => match *binop_expr {
                BinaryOpExpr(_, BinaryOperator::Minus, _) => (),
                _ => panic!("Failure: Expected binary subtraction AST node"),
            },
            _ => panic!("Failure: Expected binary expression AST node"),
        };
    }

    #[test]
    fn expr_binary_multiply() {
        let mut parser = get_parser("5 * 5");
        match parser.expr(Some(0)) {
            Ok(Expr::BinOp(binop_expr)) => match *binop_expr {
                BinaryOpExpr(_, BinaryOperator::Multiply, _) => (),
                _ => panic!("Failure: Expected binary multiply AST node"),
            },
            _ => panic!("Failure: Expected binary expression AST node"),
        };
    }

    #[test]
    fn expr_binary_int_divide() {
        let mut parser = get_parser("5 div 5");
        match parser.expr(Some(0)) {
            Ok(Expr::BinOp(binop_expr)) => match *binop_expr {
                BinaryOpExpr(_, BinaryOperator::IntegerDivide, _) => (),
                _ => panic!("Failure: Expected binary integer division AST node"),
            },
            _ => panic!("Failure: Expected binary expression AST node"),
        };
    }

    #[test]
    fn expr_binary_float_divide() {
        let mut parser = get_parser("5 / 5");
        match parser.expr(Some(0)) {
            Ok(Expr::BinOp(binop_expr)) => match *binop_expr {
                BinaryOpExpr(_, BinaryOperator::FloatDivide, _) => (),
                _ => panic!("Failure: Expected binary float division AST node"),
            },
            _ => panic!("Failure: Expected binary expression AST node"),
        };
    }

    #[test]
    fn expr_grouped() {
        let mut parser = get_parser("(5 + 5)");
        match parser.expr(Some(0)) {
            Ok(Expr::Group(grouped_expr)) => match *grouped_expr {
                GroupedExpr(_) => (),
            },
            _ => panic!("Failure: Expected grouped expression AST node"),
        };
    }

    #[test]
    fn expr_function_call() {
        let mut parser = get_parser("test()");

        assert_matches!(parser.expr(Some(0)), Ok(Expr::FunctionCall(_)));
    }

    #[test]
    fn expr_literal() {
        for literal in vec!["5", "5.5", "'test'", "true"] {
            let mut parser = get_parser(literal);

            assert_matches!(parser.expr(Some(0)), Ok(Expr::Literal(_)));
        }
    }

    #[test]
    fn expr_variable() {
        let mut parser = get_parser("test");

        assert_matches!(parser.expr(Some(0)), Ok(Expr::Variable(_)));
    }

    #[test]
    fn parse() {
        let mut parser = get_parser("program test; begin end.");

        assert_matches!(parser.parse(), Ok(Program(_, _)));
    }
}
