use super::{
    ast::{
        BinaryOpExpr, BinaryOperator, Expr, FunctionCall, GroupedExpr, Literal, UnaryOpExpr,
        UnaryOperator, Variable,
    },
    parser::Parser,
};
use crate::lexer::Token;

pub enum PrefixParselet {
    Literal,
    Grouping,
    Variable,
    UnaryOperator(u32),
}
pub enum InfixParselet {
    FunctionCall(u32),
    BinaryOperator(u32),
}

#[allow(unused_variables)]
impl PrefixParselet {
    pub fn parse(&self, parser: &mut Parser, token: &Token) -> Result<Expr, String> {
        match self {
            PrefixParselet::Literal => Ok(Expr::Literal(self.literal(parser, token)?)),
            PrefixParselet::Grouping => Ok(Expr::Group(Box::new(self.grouping(parser, token)?))),
            PrefixParselet::Variable => Ok(Expr::Variable(self.variable(parser, token)?)),
            PrefixParselet::UnaryOperator(_) => {
                Ok(Expr::UnaryOp(Box::new(self.unary_op(parser, token)?)))
            }
        }
    }

    pub fn get_precedence(&self) -> u32 {
        if let PrefixParselet::UnaryOperator(precedence) = self {
            *precedence
        } else {
            0
        }
    }

    fn literal(&self, parser: &mut Parser, token: &Token) -> Result<Literal, String> {
        match token {
            Token::Int(i) => Ok(Literal::Int(*i)),
            Token::Real(i) => Ok(Literal::Float(*i)),
            Token::String(s) => Ok(Literal::String(s.clone())),
            Token::Boolean(b) => Ok(Literal::Boolean(*b)),
            _ => Err(String::from(
                "[Parse Error (Literal)] Expected integer, real, string, or boolean const",
            )),
        }
    }

    fn grouping(&self, parser: &mut Parser, token: &Token) -> Result<GroupedExpr, String> {
        if let Token::LeftParen = token {
            match (parser.expr(None)?, parser.lexer.next()) {
                (expr, Some(Token::RightParen)) => Ok(GroupedExpr(expr)),
                (expr, _) => Err(String::from(format!(
                    "[Parse Error (Grouping)] Expected token {:?} after {:?}",
                    Token::RightParen,
                    expr
                ))),
            }
        } else {
            Err(String::from(format!(
                "[Parse Error (Grouping)] Expected token {:?}",
                Token::LeftParen
            )))
        }
    }

    fn variable(&self, parser: &mut Parser, token: &Token) -> Result<Variable, String> {
        if let Token::Id(name) = token {
            Ok(Variable(name.clone()))
        } else {
            Err(String::from("[Parse Error (Variable)] Expected token id"))
        }
    }

    fn unary_op(&self, parser: &mut Parser, token: &Token) -> Result<UnaryOpExpr, String> {
        let operator = match token {
            Token::Plus => Ok(UnaryOperator::Plus),
            Token::Minus => Ok(UnaryOperator::Minus),
            Token::Not => Ok(UnaryOperator::Not),
            _ => Err(String::from(format!(
                "[Parse Error (Unary Operator)] Expected one of {:?}",
                vec![Token::Plus, Token::Minus, Token::Not]
            ))),
        }?;

        Ok(UnaryOpExpr(
            operator,
            parser.expr(Some(self.get_precedence()))?,
        ))
    }
}

#[allow(unused_variables)]
impl InfixParselet {
    pub fn parse(&self, parser: &mut Parser, left: &Expr, token: &Token) -> Result<Expr, String> {
        match self {
            InfixParselet::BinaryOperator(_) => {
                Ok(Expr::BinOp(Box::new(self.binary_op(parser, left, token)?)))
            }
            InfixParselet::FunctionCall(_) => {
                Ok(Expr::FunctionCall(self.function_call(parser, left, token)?))
            }
        }
    }

    pub fn get_precedence(&self) -> u32 {
        let (InfixParselet::BinaryOperator(precedence) | InfixParselet::FunctionCall(precedence)) =
            self;
        *precedence
    }

    fn binary_op(
        &self,
        parser: &mut Parser,
        left: &Expr,
        token: &Token,
    ) -> Result<BinaryOpExpr, String> {
        let operator = match token {
            Token::Plus => Ok(BinaryOperator::Plus),
            Token::Minus => Ok(BinaryOperator::Minus),
            Token::Multiply => Ok(BinaryOperator::Multiply),
            Token::IntegerDiv => Ok(BinaryOperator::IntegerDivide),
            Token::FloatDiv => Ok(BinaryOperator::FloatDivide),
            Token::And => Ok(BinaryOperator::And),
            Token::Or => Ok(BinaryOperator::Or),
            Token::LessThan => Ok(BinaryOperator::LessThan),
            Token::LessOrEqual => Ok(BinaryOperator::LessThanOrEqual),
            Token::GreaterThan => Ok(BinaryOperator::GreaterThan),
            Token::GreaterOrEqual => Ok(BinaryOperator::GreaterThanOrEqual),
            Token::Equal => Ok(BinaryOperator::Equal),
            Token::NotEqual => Ok(BinaryOperator::NotEqual),
            _ => Err(String::from(format!(
                "[Parse Error (Binary Operator)] Expected one of {:?}",
                vec![
                    Token::Plus,
                    Token::Minus,
                    Token::Multiply,
                    Token::IntegerDiv,
                    Token::FloatDiv,
                    Token::And,
                    Token::Or,
                    Token::LessThan,
                    Token::LessOrEqual,
                    Token::GreaterThan,
                    Token::GreaterOrEqual,
                    Token::Equal,
                    Token::NotEqual
                ]
            ))),
        }?;

        Ok(BinaryOpExpr(
            left.clone(),
            operator,
            parser.expr(Some(self.get_precedence()))?,
        ))
    }

    fn function_call(
        &self,
        parser: &mut Parser,
        left: &Expr,
        token: &Token,
    ) -> Result<FunctionCall, String> {
        if let (Expr::Variable(name), Token::LeftParen) = (left, token) {
            match (parser.call_parameters()?, parser.lexer.next()) {
                (parameters, Some(Token::RightParen)) => Ok(FunctionCall(name.clone(), parameters)),
                (parameters, _) => Err(String::from(format!(
                    "[Parse Error (Function Call)] Expected token {:?} after {:?}",
                    Token::RightParen,
                    parameters
                ))),
            }
        } else {
            Err(String::from(format!(
                "[Parse Error (Function Call)] Expected variable token and {:?}",
                Token::LeftParen
            )))
        }
    }
}
