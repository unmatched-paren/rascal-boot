//! Rascal is a Pascal language interpreter written in Rust. Rascal-boot is a fork of
//! Rascal that aims to bootstrap the Free Pascal Compiler. This crate provides the core
//! functionality of the interpreter.

#[macro_use]
mod utils;
mod analyzer;
mod interpreter;
mod lexer;
mod parser;

use crate::analyzer::SemanticAnalyzer;
use crate::interpreter::Interpreter;
use crate::lexer::{Lexer, Source};
use crate::parser::Parser;

/// Interpret a program.
pub fn interpret(source_text: String) {
    let source = Source::new(source_text.as_str());
    let lexer = Lexer::new(source);
    let mut parser = Parser::new(lexer);

    match parser.parse() {
        Ok(program) => {
            let mut analyzer = SemanticAnalyzer::new();
            let mut interpreter = Interpreter::new();

            analyzer.analyze(&program);
            interpreter.interpret(&program);
        }
        Err(e) => println!("{}", e),
    };
}
