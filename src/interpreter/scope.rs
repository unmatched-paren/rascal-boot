use super::object::Object;
use std::{
    collections::HashMap,
    fmt::{self, Debug},
};

pub struct Scope {
    name: String,
    variables: HashMap<String, Object>,
    enclosing_scope: Option<Box<Scope>>,
}

impl Scope {
    pub fn new(name: String) -> Scope {
        Scope {
            name,
            variables: HashMap::new(),
            enclosing_scope: None,
        }
    }

    pub fn with_enclosing_scope(name: String, enclosing_scope: Scope) -> Scope {
        Scope {
            name,
            variables: HashMap::new(),
            enclosing_scope: Some(Box::new(enclosing_scope)),
        }
    }

    pub fn name(&self) -> String {
        self.name.clone()
    }

    pub fn enclosing_scope(self) -> Option<Scope> {
        if let Some(scope) = self.enclosing_scope {
            Some(*scope)
        } else {
            None
        }
    }

    pub fn get(&mut self, name: &String) -> Option<&Object> {
        if let Some(object) = self.variables.get(name) {
            Some(object)
        } else if let Some(ref mut scope) = self.enclosing_scope {
            scope.get(name)
        } else {
            None
        }
    }

    pub fn set(&mut self, name: String, object: Object) {
        self.variables.insert(name, object);
    }
}

impl Debug for Scope {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let enclosing_scope_name = if let Some(ref scope) = self.enclosing_scope {
            Some(scope.name())
        } else {
            None
        };

        write!(
            f,
            "\n\tName: {:?}\n\tVariables: {:?}\n\tEnclosing Scope: {:?}",
            self.name, self.variables, enclosing_scope_name
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::interpreter::object::Primitive;

    #[test]
    fn get() {
        let mut scope = Scope::new(String::from("test_scope"));
        scope.set(
            String::from("test_var"),
            Object::Primitive(Primitive::Integer(5)),
        );

        assert_eq!(
            scope.get(&String::from("test_var")),
            Some(&Object::Primitive(Primitive::Integer(5)))
        );
    }

    #[test]
    fn get_enclosing_scope() {
        let mut enclosing_scope = Scope::new(String::from("test_enclosing_scope"));
        enclosing_scope.set(
            String::from("test_var"),
            Object::Primitive(Primitive::Integer(5)),
        );
        let mut scope = Scope::with_enclosing_scope(String::from("test_scope"), enclosing_scope);

        assert_eq!(
            scope.get(&String::from("test_var")),
            Some(&Object::Primitive(Primitive::Integer(5)))
        );
    }

    #[test]
    fn get_local_scope() {
        let mut enclosing_scope = Scope::new(String::from("test_enclosing_scope"));
        enclosing_scope.set(
            String::from("test_var"),
            Object::Primitive(Primitive::Integer(10)),
        );
        let mut scope = Scope::with_enclosing_scope(String::from("test_scope"), enclosing_scope);
        scope.set(
            String::from("test_var"),
            Object::Primitive(Primitive::Integer(5)),
        );

        assert_eq!(
            scope.get(&String::from("test_var")),
            Some(&Object::Primitive(Primitive::Integer(5)))
        );
    }
}
