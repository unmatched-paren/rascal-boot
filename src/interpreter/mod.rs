mod built_ins;
mod interpreter;
mod object;
mod scope;

pub use interpreter::Interpreter;
