use super::{
    built_ins,
    symbol::{CallableSymbol, Symbol, VarSymbol},
    symbol_table::SymbolTable,
};
use crate::parser::ast::{
    Assignment, BinaryOpExpr, BinaryOperator, Block, CallParameters, Compound, Declarations, Expr,
    FormalParameterList, FormalParameters, FunctionCall, FunctionDeclaration, GroupedExpr,
    IfStatement, Literal, ProcedureDeclaration, Program, Statement, TypeSpec, UnaryOpExpr,
    UnaryOperator, Variable, VariableDeclaration,
};
use std::{
    fmt::{self, Display, Formatter},
    io::{self, Write},
    process,
};

pub struct SemanticAnalyzer {
    scope: Option<SymbolTable>,
}

impl Display for SemanticAnalyzer {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{:?}", self.scope)
    }
}

impl SemanticAnalyzer {
    pub fn new() -> SemanticAnalyzer {
        SemanticAnalyzer { scope: None }
    }

    pub fn analyze(&mut self, program: &Program) {
        if let Err(error) = self.visit_program(program) {
            writeln!(io::stderr(), "{}", error).unwrap();
            process::exit(1);
        }
    }

    fn init_built_ins(&mut self) -> Result<(), String> {
        self.scope()?.define(built_ins::write_procedure());
        self.scope()?.define(built_ins::writeln_procedure());
        self.scope()?.define(built_ins::readln_function());
        self.scope()?.define(built_ins::int_to_string_function());
        self.scope()?.define(built_ins::real_to_string_function());
        self.scope()?.define(built_ins::string_to_int_function());
        self.scope()?.define(built_ins::string_to_real_function());

        Ok(())
    }

    pub fn visit_program(
        &mut self,
        Program(Variable(name), block): &Program,
    ) -> Result<(), String> {
        self.enter_scope(name.to_owned());
        self.init_built_ins()?;
        self.visit_block(block)?;
        self.leave_scope();

        Ok(())
    }

    pub fn visit_block(
        &mut self,
        Block(declarations, compound): &Block,
    ) -> Result<TypeSpec, String> {
        self.visit_declarations(declarations)?;
        let return_type = self.visit_compound(compound)?;

        Ok(return_type)
    }

    pub fn visit_declarations(&mut self, node: &Vec<Declarations>) -> Result<(), String> {
        for declarations in node {
            match declarations {
                Declarations::ProcedureDeclarations(procedure_declarations) => {
                    for procedure_declaration in procedure_declarations {
                        self.visit_procedure_declaration(procedure_declaration)?;
                    }
                }
                Declarations::FunctionDeclarations(function_declarations) => {
                    for function_declaration in function_declarations {
                        self.visit_function_declaration(function_declaration)?;
                    }
                }
                Declarations::VariableDeclarations(variable_declarations) => {
                    for variable_declaration in variable_declarations {
                        self.visit_variable_declaration(variable_declaration)?;
                    }
                }
                Declarations::Empty => (),
            };
        }

        Ok(())
    }

    pub fn visit_procedure_declaration(
        &mut self,
        ProcedureDeclaration(name, parameter_list, block): &ProcedureDeclaration,
    ) -> Result<(), String> {
        let parameters = self.visit_formal_parameter_list(parameter_list)?;

        self.scope()?
            .define(Symbol::Callable(CallableSymbol::Procedure(
                name.to_owned(),
                parameters.to_vec(),
            )));
        self.enter_scope(name.to_owned());

        for parameter in parameters {
            self.scope()?.define(Symbol::Var(parameter));
        }

        self.visit_block(block)?;
        self.leave_scope();

        Ok(())
    }

    pub fn visit_function_declaration(
        &mut self,
        FunctionDeclaration(name, parameter_list, block, return_type): &FunctionDeclaration,
    ) -> Result<(), String> {
        let parameters = self.visit_formal_parameter_list(parameter_list)?;

        self.scope()?
            .define(Symbol::Callable(CallableSymbol::Function(
                name.to_owned(),
                parameters.to_vec(),
                return_type.clone(),
            )));
        self.enter_scope(name.to_owned());

        for parameter in parameters {
            self.scope()?.define(Symbol::Var(parameter));
        }

        match (self.visit_block(block)?, return_type) {
                    (TypeSpec::String, TypeSpec::String)   => Ok(()),
                    (TypeSpec::Integer, TypeSpec::Integer) => Ok(()),
                    (TypeSpec::Real, TypeSpec::Real)       => Ok(()),
                    (TypeSpec::Boolean, TypeSpec::Boolean) => Ok(()),
                    (actual, expected)                     => Err(String::from(
                        format!("[Analysis Error] Mismatching return types in function '{:?}'. Declared return type of {:?}, actual return type of {:?}", name, expected, actual)
                    ))
                }?;

        self.leave_scope();

        Ok(())
    }

    pub fn visit_formal_parameter_list(
        &mut self,
        FormalParameterList(formal_parameters): &FormalParameterList,
    ) -> Result<Vec<VarSymbol>, String> {
        let mut vars: Vec<VarSymbol> = vec![];
        for parameters in formal_parameters {
            let mut other_vars = self.visit_formal_parameters(parameters)?;
            vars.append(&mut other_vars);
        }
        Ok(vars.to_vec())
    }

    pub fn visit_formal_parameters(
        &mut self,
        node: &FormalParameters,
    ) -> Result<Vec<VarSymbol>, String> {
        let mut vars: Vec<VarSymbol> = vec![];

        match node {
            FormalParameters(names, type_spec) => match type_spec {
                TypeSpec::Integer => Ok(names
                    .iter()
                    .for_each(|name| vars.push(VarSymbol::Integer(name.to_owned())))),
                TypeSpec::Real => Ok(names
                    .iter()
                    .for_each(|name| vars.push(VarSymbol::Real(name.to_owned())))),
                TypeSpec::String => Ok(names
                    .iter()
                    .for_each(|name| vars.push(VarSymbol::String(name.to_owned())))),
                TypeSpec::Boolean => Ok(names
                    .iter()
                    .for_each(|name| vars.push(VarSymbol::Boolean(name.to_owned())))),
                TypeSpec::Unit => Err(String::from(
                    "[Analysis Error] Formal Parameter of type Unit",
                )),
            },
        }?;

        Ok(vars.to_vec())
    }

    pub fn visit_variable_declaration(
        &mut self,
        VariableDeclaration(names, typespec): &VariableDeclaration,
    ) -> Result<(), String> {
        for name in names {
            if let Some(_) = self.scope()?.local_lookup(name) {
                Err(String::from(format!(
                    "[Analysis Error] Variable declared more than once: {}",
                    name
                )))
            } else {
                match typespec {
                    TypeSpec::Integer => Ok(self
                        .scope()?
                        .define(Symbol::Var(VarSymbol::Integer(name.to_owned())))),
                    TypeSpec::Real => Ok(self
                        .scope()?
                        .define(Symbol::Var(VarSymbol::Real(name.to_owned())))),
                    TypeSpec::String => Ok(self
                        .scope()?
                        .define(Symbol::Var(VarSymbol::String(name.to_owned())))),
                    TypeSpec::Boolean => Ok(self
                        .scope()?
                        .define(Symbol::Var(VarSymbol::Boolean(name.to_owned())))),
                    TypeSpec::Unit => Err(String::from(
                        "[Analysis Error] Variable Declaration of type Unit",
                    )),
                }
            }?;
        }

        Ok(())
    }

    pub fn visit_compound(&mut self, Compound(statements): &Compound) -> Result<TypeSpec, String> {
        let mut last_type = TypeSpec::Unit;
        for statement in statements {
            last_type = self.visit_statement(statement)?;
        }

        Ok(last_type)
    }

    pub fn visit_statement(&mut self, node: &Statement) -> Result<TypeSpec, String> {
        match node {
            Statement::Compound(compound) => self.visit_compound(compound),
            Statement::Assignment(assignment) => self.visit_assignment(assignment),
            Statement::IfStatement(if_statement) => self.visit_if_statement(if_statement),
            Statement::FunctionCall(function_call) => self.visit_function_call(function_call),
        }
    }

    pub fn visit_if_statement(&mut self, node: &IfStatement) -> Result<TypeSpec, String> {
        match node {
            IfStatement::If(expr, compound_statement) => {
                match self.visit_expr(expr)? {
                    TypeSpec::Boolean => Ok(()),
                    _ => Err(String::from(
                        "[Analysis Error] If statement must use a boolean expression",
                    )),
                }?;

                self.visit_compound(compound_statement)?;
                Ok(TypeSpec::Unit)
            }
            IfStatement::IfElse(expr, if_compound_statement, else_compound_statement) => {
                match self.visit_expr(expr)? {
                    TypeSpec::Boolean => Ok(()),
                    _ => Err(String::from(
                        "[Analysis Error] If statement must use a boolean expression",
                    )),
                }?;

                self.visit_compound(if_compound_statement)?;
                self.visit_compound(else_compound_statement)?;
                Ok(TypeSpec::Unit)
            }
            IfStatement::IfElseIf(expr, if_compound_statement, else_if_statement) => {
                match self.visit_expr(expr)? {
                    TypeSpec::Boolean => Ok(()),
                    _ => Err(String::from(
                        "[Analysis Error] If statement must use a boolean expression",
                    )),
                }?;

                self.visit_compound(if_compound_statement)?;
                self.visit_if_statement(else_if_statement)?;
                Ok(TypeSpec::Unit)
            }
        }
    }

    pub fn visit_assignment(
        &mut self,
        Assignment(Variable(name), expression): &Assignment,
    ) -> Result<TypeSpec, String> {
        let symbol = if let Some(symbol) = self.scope()?.lookup(name) {
            Ok(symbol.clone())
        } else {
            Err(String::from(format!(
                "[Analysis Error] Variable {} was never defined",
                name
            )))
        }?;

        match (symbol, self.visit_expr(expression)?) {
                    (Symbol::Var(VarSymbol::Integer(_)), TypeSpec::Integer) => Ok(TypeSpec::Integer),
                    (Symbol::Var(VarSymbol::Real(_)), TypeSpec::Real)       => Ok(TypeSpec::Real),
                    (Symbol::Var(VarSymbol::String(_)), TypeSpec::String)   => Ok(TypeSpec::String),
                    (Symbol::Var(VarSymbol::Boolean(_)), TypeSpec::Boolean) => Ok(TypeSpec::Boolean),
                    (Symbol::Var(var_symbol), TypeSpec::Unit)               => Err(String::from(format!("[Analysis Error] Attempted to assign {:?} to a statement with no return type", var_symbol))),
                    (Symbol::Var(var_symbol), type_spec)                    => Err(String::from(format!("[Analysis Error] Mismatched Types, Attempted to assign {:?} to {:?}", type_spec, var_symbol))),
                    (Symbol::Callable(callable), _)                         => Err(String::from(format!("[Analysis Error] Attempted to assign expression to callable {:?}", callable)))
                }
    }

    pub fn visit_function_call(
        &mut self,
        FunctionCall(Variable(name), parameters): &FunctionCall,
    ) -> Result<TypeSpec, String> {
        let callable = if let Some(Symbol::Callable(callable)) = self.scope()?.lookup(name) {
            Ok(callable.clone())
        } else {
            Err(String::from(format!(
                "[Analysis Error] Unknown callable '{:?}'",
                name
            )))
        }?;
        let declared_params = match callable {
            CallableSymbol::Procedure(_, ref declared_params) => declared_params.to_vec(),
            CallableSymbol::Function(_, ref declared_params, _) => declared_params.to_vec(),
        };
        let given_params = self.visit_call_parameters(parameters)?;

        if declared_params.len() == given_params.len() {
            for (declared, given) in declared_params.iter().zip(given_params.iter()) {
                match (declared, given) {
                            (VarSymbol::Integer(_), TypeSpec::Integer) => Ok(()),
                            (VarSymbol::Real(_), TypeSpec::Real)       => Ok(()),
                            (VarSymbol::String(_), TypeSpec::String)   => Ok(()),
                            (VarSymbol::Boolean(_), TypeSpec::Boolean) => Ok(()),
                            (expected, actual)                         => Err(String::from(format!("[Analysis Error] Callable expected parameter of type {:?}, but {:?} was given", expected, actual)))
                        }?;
            }
            match callable {
                CallableSymbol::Procedure(_, _) => Ok(TypeSpec::Unit),
                CallableSymbol::Function(_, _, type_spec) => Ok(type_spec),
            }
        } else {
            Err(String::from(format!("[Analysis Error] Callable {:?} expected {:?} arguments, but {:?} were given", callable, declared_params.len(), given_params.len())))
        }
    }

    pub fn visit_call_parameters(
        &mut self,
        CallParameters(expressions): &CallParameters,
    ) -> Result<Vec<TypeSpec>, String> {
        let mut parameters: Vec<TypeSpec> = vec![];

        for expr in expressions.iter() {
            parameters.push(self.visit_expr(expr)?);
        }

        Ok(parameters)
    }

    pub fn visit_expr(&mut self, node: &Expr) -> Result<TypeSpec, String> {
        match node {
            Expr::UnaryOp(unaryop_expr) => self.visit_unaryop(unaryop_expr),
            Expr::BinOp(binop_expr) => self.visit_binop(binop_expr),
            Expr::Group(group_expr) => self.visit_group(group_expr),
            Expr::Literal(literal) => self.visit_literal(literal),
            Expr::Variable(variable) => self.visit_variable(variable),
            Expr::FunctionCall(function_call) => self.visit_function_call(function_call),
        }
    }

    pub fn visit_unaryop(
        &mut self,
        UnaryOpExpr(operator, unary_expr): &UnaryOpExpr,
    ) -> Result<TypeSpec, String> {
        match (operator, self.visit_expr(unary_expr)?) {
            (UnaryOperator::Plus, TypeSpec::Integer)  => Ok(TypeSpec::Integer),
            (UnaryOperator::Minus, TypeSpec::Integer) => Ok(TypeSpec::Integer),
            (UnaryOperator::Plus, TypeSpec::Real)     => Ok(TypeSpec::Real),
            (UnaryOperator::Minus, TypeSpec::Real)    => Ok(TypeSpec::Real),
            (UnaryOperator::Not, TypeSpec::Boolean)   => Ok(TypeSpec::Boolean),
            (operator, type_spec) =>
		Err(String::from(
                      format!("[Analysis Error] Attempted to use unary operator {:?} with incompatible type {:?}", operator, type_spec)
                ))
	}
    }

    pub fn visit_binop(
        &mut self,
        BinaryOpExpr(left, operator, right): &BinaryOpExpr,
    ) -> Result<TypeSpec, String> {
        match (self.visit_expr(left)?, operator, self.visit_expr(right)?) {
                    (TypeSpec::Integer, BinaryOperator::Plus, TypeSpec::Integer)               => Ok(TypeSpec::Integer),
                    (TypeSpec::Integer, BinaryOperator::Minus, TypeSpec::Integer)              => Ok(TypeSpec::Integer),
                    (TypeSpec::Integer, BinaryOperator::Multiply, TypeSpec::Integer)           => Ok(TypeSpec::Integer),
                    (TypeSpec::Integer, BinaryOperator::IntegerDivide, TypeSpec::Integer)      => Ok(TypeSpec::Integer),
                    (TypeSpec::Integer, BinaryOperator::LessThan, TypeSpec::Integer)           => Ok(TypeSpec::Boolean),
                    (TypeSpec::Integer, BinaryOperator::LessThanOrEqual, TypeSpec::Integer)    => Ok(TypeSpec::Boolean),
                    (TypeSpec::Integer, BinaryOperator::GreaterThan, TypeSpec::Integer)        => Ok(TypeSpec::Boolean),
                    (TypeSpec::Integer, BinaryOperator::GreaterThanOrEqual, TypeSpec::Integer) => Ok(TypeSpec::Boolean),
                    (TypeSpec::Integer, BinaryOperator::Equal, TypeSpec::Integer)              => Ok(TypeSpec::Boolean),
                    (TypeSpec::Integer, BinaryOperator::NotEqual, TypeSpec::Integer)           => Ok(TypeSpec::Boolean),

                    (TypeSpec::Real, BinaryOperator::Plus, TypeSpec::Real)                     => Ok(TypeSpec::Real),
                    (TypeSpec::Real, BinaryOperator::Minus, TypeSpec::Real)                    => Ok(TypeSpec::Real),
                    (TypeSpec::Real, BinaryOperator::Multiply, TypeSpec::Real)                 => Ok(TypeSpec::Real),
                    (TypeSpec::Real, BinaryOperator::FloatDivide, TypeSpec::Real)              => Ok(TypeSpec::Real),
                    (TypeSpec::Real, BinaryOperator::LessThan, TypeSpec::Real)                 => Ok(TypeSpec::Boolean),
                    (TypeSpec::Real, BinaryOperator::LessThanOrEqual, TypeSpec::Real)          => Ok(TypeSpec::Boolean),
                    (TypeSpec::Real, BinaryOperator::GreaterThan, TypeSpec::Real)              => Ok(TypeSpec::Boolean),
                    (TypeSpec::Real, BinaryOperator::GreaterThanOrEqual, TypeSpec::Real)       => Ok(TypeSpec::Boolean),
                    (TypeSpec::Real, BinaryOperator::Equal, TypeSpec::Real)                    => Ok(TypeSpec::Boolean),
                    (TypeSpec::Real, BinaryOperator::NotEqual, TypeSpec::Real)                 => Ok(TypeSpec::Boolean),

                    (TypeSpec::String, BinaryOperator::Plus, TypeSpec::String)                 => Ok(TypeSpec::String),
                    (TypeSpec::Boolean, BinaryOperator::And, TypeSpec::Boolean)                => Ok(TypeSpec::Boolean),
                    (TypeSpec::Boolean, BinaryOperator::Or, TypeSpec::Boolean)                 => Ok(TypeSpec::Boolean),

                    (TypeSpec::Integer, operator, TypeSpec::Integer)                            => Err(String::from(format!("[Analysis Error] Binary operator {:?} is incompatible with {:?} types", operator, TypeSpec::Integer))),
                    (TypeSpec::Real, operator, TypeSpec::Real)                                  => Err(String::from(format!("[Analysis Error] Binary operator {:?} is incompatible with {:?} types", operator, TypeSpec::Real))),
                    (TypeSpec::String, operator, TypeSpec::String)                              => Err(String::from(format!("[Analysis Error] Binary operator {:?} is incompatible with {:?} types", operator, TypeSpec::String))),
                    (TypeSpec::Boolean, operator, TypeSpec::Boolean)                            => Err(String::from(format!("[Analysis Error] Binary operator {:?} is incompatible with {:?} types", operator, TypeSpec::Boolean))),
                    (type_spec_left, operator, type_spec_right)                                 => Err(String::from(format!("[Analysis Error] Attempted to use binary operator {:?} with mismatching types {:?} and {:?}", operator, type_spec_left, type_spec_right)))
                }
    }

    fn visit_group(&mut self, GroupedExpr(grouped_expr): &GroupedExpr) -> Result<TypeSpec, String> {
        self.visit_expr(grouped_expr)
    }

    pub fn visit_literal(&mut self, expr: &Literal) -> Result<TypeSpec, String> {
        match expr {
            Literal::Int(_) => Ok(TypeSpec::Integer),
            Literal::Float(_) => Ok(TypeSpec::Real),
            Literal::String(_) => Ok(TypeSpec::String),
            Literal::Boolean(_) => Ok(TypeSpec::Boolean),
        }
    }

    pub fn visit_variable(&mut self, Variable(name): &Variable) -> Result<TypeSpec, String> {
        if let Some(symbol) = self.scope()?.lookup(name) {
            match symbol {
                Symbol::Var(VarSymbol::Integer(_)) => Ok(TypeSpec::Integer),
                Symbol::Var(VarSymbol::Real(_)) => Ok(TypeSpec::Real),
                Symbol::Var(VarSymbol::String(_)) => Ok(TypeSpec::String),
                Symbol::Var(VarSymbol::Boolean(_)) => Ok(TypeSpec::Boolean),
                Symbol::Callable(callable) => Err(String::from(format!(
                    "[Analysis Error] Attempted to use callable {:?} as a variable",
                    callable
                ))),
            }
        } else {
            Err(String::from(format!(
                "[Analysis Error] Unknown variable with name {}",
                name
            )))
        }
    }

    pub fn enter_scope(&mut self, name: String) {
        if let Some(scope) = self.scope.take() {
            self.scope = Some(SymbolTable::with_enclosing_scope(name, scope))
        } else {
            self.scope = Some(SymbolTable::new(name))
        };
    }

    pub fn leave_scope(&mut self) {
        if let Some(scope) = self.scope.take() {
            self.scope = scope.enclosing_scope()
        } else {
            self.scope = None
        };
    }

    pub fn scope(&mut self) -> Result<&mut SymbolTable, String> {
        if let Some(ref mut scope) = self.scope {
            Ok(scope)
        } else {
            Err(String::from(
                "[Analysis Error] Unknown Scope",
            ))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn visit_program() {
        let mut analyzer = SemanticAnalyzer::new();
        let program = Program(
            Variable(String::from("test")),
            Block(vec![], Compound(vec![])),
        );

        assert_eq!(analyzer.visit_program(&program), Ok(()));
    }

    #[test]
    fn visit_block() {
        let mut analyzer = SemanticAnalyzer::new();
        let block = Block(vec![], Compound(vec![]));

        assert_eq!(analyzer.visit_block(&block), Ok(TypeSpec::Unit));
    }

    #[test]
    fn visit_declarations() {
        let mut analyzer = SemanticAnalyzer::new();
        let declarations = vec![
            Declarations::VariableDeclarations(vec![]),
            Declarations::FunctionDeclarations(vec![]),
            Declarations::ProcedureDeclarations(vec![]),
            Declarations::Empty,
        ];

        assert_eq!(analyzer.visit_declarations(&declarations), Ok(()));
    }

    #[test]
    fn visit_procedure_declaration() {
        let mut analyzer = SemanticAnalyzer::new();
        analyzer.enter_scope(String::from("test_scope"));

        let procedure_declaration = ProcedureDeclaration(
            String::from("test"),
            FormalParameterList(vec![]),
            Block(vec![], Compound(vec![])),
        );

        assert_eq!(
            analyzer.visit_procedure_declaration(&procedure_declaration),
            Ok(())
        );
        assert_eq!(
            analyzer.scope().unwrap().lookup(&String::from("test")),
            Some(&Symbol::Callable(CallableSymbol::Procedure(
                String::from("test"),
                vec![]
            )))
        );
    }

    #[test]
    fn visit_function_declaration() {
        let mut analyzer = SemanticAnalyzer::new();
        analyzer.enter_scope(String::from("test_scope"));

        let function_declaration = FunctionDeclaration(
            String::from("test"),
            FormalParameterList(vec![]),
            Block(
                vec![
                    // declare variable 'foobar' of type integer
                    Declarations::VariableDeclarations(vec![VariableDeclaration(
                        vec![String::from("foobar")],
                        TypeSpec::Integer,
                    )]),
                ],
                Compound(vec![Statement::Assignment(
                    // assign variable 'foobar' to integer 5 (makes return value of function an integer)
                    Assignment(
                        Variable(String::from("foobar")),
                        Expr::Literal(Literal::Int(5)),
                    ),
                )]),
            ),
            TypeSpec::Integer,
        );

        assert_eq!(
            analyzer.visit_function_declaration(&function_declaration),
            Ok(())
        );
        assert_eq!(
            analyzer.scope().unwrap().lookup(&String::from("test")),
            Some(&Symbol::Callable(CallableSymbol::Function(
                String::from("test"),
                vec![],
                TypeSpec::Integer
            )))
        );
    }

    #[test]
    fn visit_formal_parameter_list() {
        let mut analyzer = SemanticAnalyzer::new();
        let formal_parameter_list = FormalParameterList(vec![
            FormalParameters(
                vec![String::from("a"), String::from("b")],
                TypeSpec::Integer,
            ),
            FormalParameters(vec![String::from("c"), String::from("d")], TypeSpec::Real),
            FormalParameters(vec![String::from("e"), String::from("f")], TypeSpec::String),
            FormalParameters(
                vec![String::from("g"), String::from("h")],
                TypeSpec::Boolean,
            ),
        ]);

        assert_eq!(
            analyzer.visit_formal_parameter_list(&formal_parameter_list),
            Ok(vec![
                VarSymbol::Integer(String::from("a")),
                VarSymbol::Integer(String::from("b")),
                VarSymbol::Real(String::from("c")),
                VarSymbol::Real(String::from("d")),
                VarSymbol::String(String::from("e")),
                VarSymbol::String(String::from("f")),
                VarSymbol::Boolean(String::from("g")),
                VarSymbol::Boolean(String::from("h")),
            ])
        );
    }

    #[test]
    fn visit_formal_parameters() {
        let mut analyzer = SemanticAnalyzer::new();
        let formal_parameters = FormalParameters(
            vec![String::from("a"), String::from("b")],
            TypeSpec::Integer,
        );

        assert_eq!(
            analyzer.visit_formal_parameters(&formal_parameters),
            Ok(vec![
                VarSymbol::Integer(String::from("a")),
                VarSymbol::Integer(String::from("b"))
            ])
        );
    }

    #[test]
    fn visit_variable_declaration() {
        let mut analyzer = SemanticAnalyzer::new();
        analyzer.enter_scope(String::from("test_scope"));
        let variable_declaration = VariableDeclaration(
            vec![String::from("a"), String::from("b")],
            TypeSpec::Integer,
        );

        assert_eq!(
            analyzer.visit_variable_declaration(&variable_declaration),
            Ok(())
        );
        assert_eq!(
            analyzer.scope().unwrap().lookup(&String::from("a")),
            Some(&Symbol::Var(VarSymbol::Integer(String::from("a"))))
        );
        assert_eq!(
            analyzer.scope().unwrap().lookup(&String::from("b")),
            Some(&Symbol::Var(VarSymbol::Integer(String::from("b"))))
        );
    }

    #[test]
    fn visit_compound() {
        let mut analyzer = SemanticAnalyzer::new();
        analyzer.enter_scope(String::from("test_scope"));
        analyzer
            .scope()
            .unwrap()
            .define(Symbol::Var(VarSymbol::Integer(String::from("foo"))));
        analyzer
            .scope()
            .unwrap()
            .define(Symbol::Var(VarSymbol::Boolean(String::from("bar"))));

        let compound = Compound(vec![
            Statement::Assignment(Assignment(
                Variable(String::from("foo")),
                Expr::Literal(Literal::Int(5)),
            )),
            Statement::Assignment(Assignment(
                Variable(String::from("bar")),
                Expr::Literal(Literal::Boolean(true)),
            )),
        ]);

        assert_eq!(analyzer.visit_compound(&compound), Ok(TypeSpec::Boolean));
    }

    #[test]
    fn visit_if_statement() {
        let mut analyzer = SemanticAnalyzer::new();
        let if_statement = IfStatement::IfElseIf(
            // if false then
            Expr::Literal(Literal::Boolean(false)), //     begin
            Compound(vec![]),                       //     end
            Box::new(
                // else if true then
                IfStatement::IfElse(
                    //     begin
                    Expr::Literal(Literal::Boolean(true)), //     end
                    Compound(vec![]),                      // else
                    Compound(vec![]),                      //     begin
                ), //     end
            ),
        );

        assert_eq!(
            analyzer.visit_if_statement(&if_statement),
            Ok(TypeSpec::Unit)
        );
    }

    #[test]
    fn visit_assignment() {
        let mut analyzer = SemanticAnalyzer::new();
        analyzer.enter_scope(String::from("test_scope"));
        analyzer
            .scope()
            .unwrap()
            .define(Symbol::Var(VarSymbol::Integer(String::from("test"))));
        let assignment = Assignment(
            Variable(String::from("test")),
            Expr::Literal(Literal::Int(5)),
        );

        assert_eq!(
            analyzer.visit_assignment(&assignment),
            Ok(TypeSpec::Integer)
        );
    }

    #[test]
    fn visit_function_call() {
        let mut analyzer = SemanticAnalyzer::new();
        analyzer.enter_scope(String::from("test_scope"));
        analyzer
            .scope()
            .unwrap()
            .define(Symbol::Callable(CallableSymbol::Function(
                String::from("test"),
                vec![
                    VarSymbol::Integer(String::from("a")),
                    VarSymbol::Integer(String::from("b")),
                    VarSymbol::Boolean(String::from("c")),
                ],
                TypeSpec::Real,
            )));
        let function_call = FunctionCall(
            Variable(String::from("test")),
            CallParameters(vec![
                Expr::Literal(Literal::Int(5)),
                Expr::Literal(Literal::Int(10)),
                Expr::Literal(Literal::Boolean(true)),
            ]),
        );

        assert_eq!(
            analyzer.visit_function_call(&function_call),
            Ok(TypeSpec::Real)
        );
    }

    #[test]
    fn visit_call_parameters() {
        let mut analyzer = SemanticAnalyzer::new();
        let call_parameters = CallParameters(vec![
            Expr::Literal(Literal::Int(5)),
            Expr::Literal(Literal::Int(10)),
            Expr::Literal(Literal::Boolean(true)),
        ]);

        assert_eq!(
            analyzer.visit_call_parameters(&call_parameters),
            Ok(vec![
                TypeSpec::Integer,
                TypeSpec::Integer,
                TypeSpec::Boolean
            ])
        );
    }

    #[test]
    fn visit_unaryop_expr() {
        let mut analyzer = SemanticAnalyzer::new();
        let unaryop_expr = UnaryOpExpr(UnaryOperator::Minus, Expr::Literal(Literal::Float(5.0)));

        assert_eq!(analyzer.visit_unaryop(&unaryop_expr), Ok(TypeSpec::Real));
    }

    #[test]
    fn visit_binop_expr() {
        let mut analyzer = SemanticAnalyzer::new();
        let binop_expr = BinaryOpExpr(
            Expr::Literal(Literal::Int(5)),
            BinaryOperator::Multiply,
            Expr::Literal(Literal::Int(10)),
        );

        assert_eq!(analyzer.visit_binop(&binop_expr), Ok(TypeSpec::Integer));
    }

    #[test]
    fn visit_group_expr() {
        let mut analyzer = SemanticAnalyzer::new();
        let group_expr = GroupedExpr(Expr::Literal(Literal::Boolean(true)));

        assert_eq!(analyzer.visit_group(&group_expr), Ok(TypeSpec::Boolean));
    }

    #[test]
    fn visit_literal() {
        let mut analyzer = SemanticAnalyzer::new();
        let literal = Literal::String(String::from("test"));

        assert_eq!(analyzer.visit_literal(&literal), Ok(TypeSpec::String));
    }

    #[test]
    fn visit_variable() {
        let mut analyzer = SemanticAnalyzer::new();
        analyzer.enter_scope(String::from("test_scope"));
        analyzer
            .scope()
            .unwrap()
            .define(Symbol::Var(VarSymbol::Real(String::from("test"))));
        let variable = Variable(String::from("test"));

        assert_eq!(analyzer.visit_variable(&variable), Ok(TypeSpec::Real));
    }
}
