use super::symbol::{CallableSymbol, Symbol, VarSymbol};
use crate::parser::ast::TypeSpec;

pub fn write_procedure() -> Symbol {
    Symbol::Callable(CallableSymbol::Procedure(
        String::from("write"),
        vec![VarSymbol::String(String::from("text"))],
    ))
}

pub fn writeln_procedure() -> Symbol {
    Symbol::Callable(CallableSymbol::Procedure(
        String::from("writeln"),
        vec![VarSymbol::String(String::from("text"))],
    ))
}

pub fn readln_function() -> Symbol {
    Symbol::Callable(CallableSymbol::Function(
        String::from("readln"),
        vec![],
        TypeSpec::String,
    ))
}

pub fn int_to_string_function() -> Symbol {
    Symbol::Callable(CallableSymbol::Function(
        String::from("IntToString"),
        vec![VarSymbol::Integer(String::from("value"))],
        TypeSpec::String,
    ))
}

pub fn real_to_string_function() -> Symbol {
    Symbol::Callable(CallableSymbol::Function(
        String::from("RealToString"),
        vec![VarSymbol::Real(String::from("value"))],
        TypeSpec::String,
    ))
}

pub fn string_to_int_function() -> Symbol {
    Symbol::Callable(CallableSymbol::Function(
        String::from("StringToInt"),
        vec![VarSymbol::String(String::from("text"))],
        TypeSpec::Integer,
    ))
}

pub fn string_to_real_function() -> Symbol {
    Symbol::Callable(CallableSymbol::Function(
        String::from("StringToReal"),
        vec![VarSymbol::String(String::from("text"))],
        TypeSpec::Real,
    ))
}
