use crate::parser::ast::TypeSpec;
use std::clone::Clone;

#[derive(Debug, Clone, PartialEq)]
pub enum Symbol {
    Var(VarSymbol),
    Callable(CallableSymbol),
}

#[derive(Debug, Clone, PartialEq)]
pub enum VarSymbol {
    Integer(String),
    Real(String),
    String(String),
    Boolean(String),
}

#[derive(Debug, Clone, PartialEq)]
pub enum CallableSymbol {
    Procedure(String, Vec<VarSymbol>),
    Function(String, Vec<VarSymbol>, TypeSpec),
}

impl Symbol {
    pub fn name(&self) -> String {
        match self {
            Symbol::Var(VarSymbol::Integer(name))
            | Symbol::Var(VarSymbol::Real(name))
            | Symbol::Var(VarSymbol::String(name))
            | Symbol::Var(VarSymbol::Boolean(name))
            | Symbol::Callable(CallableSymbol::Procedure(name, _))
            | Symbol::Callable(CallableSymbol::Function(name, _, _)) => name.clone(),
        }
    }
}
