//! Provides utility macros for Rascal.
#![allow(unused_macros)]

macro_rules! assert_matches {
    ($expression:expr, $($pattern:pat_param)|*) => {
        match $expression {
            $($pattern)|* => (),
            ref actual    => panic!("\nExpected: {}\nActual: {:?}\n", stringify!($($pattern)|*), actual)
        }
    };
}
