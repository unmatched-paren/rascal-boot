use gag::BufferRedirect;
use std::io::Read;

// Asserts that the Pascal program `src` writes `expected_output` to stdout.
pub fn test_program_output(src: &str, expected_output: &str) {
    let mut redirect = BufferRedirect::stdout().expect("Failed to create the redirect tmpfile!");
    rascal_boot::interpret(src.to_string());
    let mut actual_output = String::new();
    redirect
        .read_to_string(&mut actual_output)
        .expect("Failed to read the redirect tmpfile!");
    assert_eq!(actual_output.as_str(), expected_output);
    drop(redirect);
}

#[allow(unused_macros)] // not sure why this counts as unused...
macro_rules! test_output {
    ($src:expr, $out:expr) => {
        const SRC: &str = include_str!($src);
        #[test]
        fn test() {
            common::test_program_output(SRC, $out);
        }
    };
}
