
program variables;

var
   x : string;
   y : string;

begin
   x := 'hi';
   y := 'friends';

   write(x);
   write(', ');
   writeln(y);

   x := 'hola';
   y := 'amigos';
   write(x);
   write(', ');
   writeln(y);
end.
