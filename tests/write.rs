#[macro_use]
mod common;

test_output!(
    "write.pas",
    "Hello, world!\nThis is Rascal-Boot speaking! Foo bar baz!\n"
);
